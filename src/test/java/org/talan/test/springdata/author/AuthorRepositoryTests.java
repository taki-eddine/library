package org.talan.test.springdata.author;

import javax.inject.Inject;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.Transactional;
import org.talan.springdata.config.PersistenceContext;
import org.talan.springdata.config.entity.Author;
import org.talan.springdata.service.AuthorService;

@Configuration
@Transactional
@Import(PersistenceContext.class)
public class AuthorRepositoryTests {

    @Inject
    private AuthorService authorRepository;

    public static void main(String[] args) {
        try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();) {
            ctx.register(AuthorRepositoryTests.class);
            ctx.refresh();

            AuthorRepositoryTests authorRepositoryTester = (AuthorRepositoryTests) ctx.getBean("authorRepositoryTests");
            authorRepositoryTester.addAuthors();
        }
    }

    public void addAuthors() {
        Author author;

        author = new Author();
        author.setName("Jack");
        authorRepository.save(author);

        author = new Author();
        author.setName("Susan");
        authorRepository.save(author);
    }

}
