package org.talan.jest.entity;

import io.searchbox.annotations.JestId;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Person {

    @JestId
    @Getter @Setter
    private String id;

    @Getter @Setter
    private String name;

    @Getter @Setter
    private String age;

    // TODO: Add method `isValide()`, to validate if the instance is ready to be inserted in the DB.

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
