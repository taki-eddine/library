package org.talan.jest.repository;

import java.io.IOException;
import java.util.List;
import javax.inject.Named;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.talan.jest.entity.Person;
import org.talan.jest.internal.repository.CrudRepository;

@Named
public class PersonRepository extends CrudRepository<Person> {

    private static final Logger LOGGER = Logger.getLogger(PersonRepository.class);

    public PersonRepository() {
        super("persons", "person", Person.class);
    }

    public List<Person> findAllByName(String name) throws IOException {
        String searchQuery = new JSONObject().put("query", new JSONObject()
                                                  .put("match_phrase_prefix", new JSONObject()
                                                       .put("name", name + "*")))
                                             .toString();
        return findAllByQuery(searchQuery);
    }

}
