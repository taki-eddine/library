package org.talan.jest.internal.repository;

import io.searchbox.client.JestClient;
import io.searchbox.core.Index;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.talan.jest.repository.PersonRepository;

public abstract class CrudRepository<T> {

    private static final Logger LOGGER = Logger.getLogger(PersonRepository.class);

    protected final String INDEX;
    protected final String TYPE;

    private final Class<T> sourceType;

    @Inject
    protected JestClient jestClient;


    public CrudRepository(String index, String type, Class<T> clazz) {
        INDEX = index;
        TYPE = type;
        sourceType = clazz;
    }


    protected SearchResult search(String searchQuery) throws IOException {
        Search search = new Search.Builder(searchQuery).addIndex(INDEX).addType(TYPE).build();

        SearchResult result = jestClient.execute(search);
        return result;
    }


    protected Optional<T> findFirstByQuery(String searchQuery) throws IOException {
        SearchResult.Hit<T, Void> resultHit = search(searchQuery).getFirstHit(sourceType);
        Optional<T> entity = Optional.empty();

        if ( resultHit != null ) {
            entity = Optional.of(resultHit.source);
        }

        return entity;
    }


    protected List<T> findAllByQuery(String searchQuery) throws IOException {
        List<SearchResult.Hit<T, Void>> hits = search(searchQuery).getHits(sourceType);
        List<T> entities = hits.stream().map(hit -> hit.source).collect(Collectors.toList());
        return entities;
    }


    public void save(T entity) throws IOException {
        Index index = new Index.Builder(entity).index(INDEX).type(TYPE).build();
        jestClient.execute(index);
    }


    public Optional<T> findOne(String id) throws IOException {
        String searchQuery = new JSONObject().put("query", new JSONObject()
                                                  .put("match", new JSONObject()
                                                       .put("_id", id)))
                                             .toString();
        return findFirstByQuery(searchQuery);
    }


    public List<T> findAll() throws IOException {
        String searchQuery = new JSONObject().put("query", new JSONObject()
                                                  .put("match_all", new JSONObject()))
                                             .toString();
        return findAllByQuery(searchQuery);
    }

}
