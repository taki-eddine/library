package org.talan.jest.config;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JestConfiguration {

    @Bean
    public JestClient jestClient() {
        JestClientFactory jestClientFactory = new JestClientFactory();

        jestClientFactory.setHttpClientConfig(new HttpClientConfig.Builder("http://localhost:9200")
                .multiThreaded(true)
                .build());

        return jestClientFactory.getObject();
    }

}
