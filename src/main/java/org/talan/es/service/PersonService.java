package org.talan.es.service;

import java.io.IOException;
import java.util.List;
import org.talan.jest.entity.Person;

public interface PersonService {

    void save(Person entity) throws IOException;

    List<Person> findAll() throws IOException;

    List<Person> findAllByName(String name) throws IOException;

}
