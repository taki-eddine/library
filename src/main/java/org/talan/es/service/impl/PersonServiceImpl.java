package org.talan.es.service.impl;

import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.log4j.Logger;
import org.talan.es.service.PersonService;
import org.talan.jest.entity.Person;
import org.talan.jest.repository.PersonRepository;

@Named
public class PersonServiceImpl implements PersonService {

    private static final Logger LOGGER = Logger.getLogger(PersonServiceImpl.class);

    @Inject
    private PersonRepository personRepository;


    @Override
    public void save(Person entity) throws IOException {
        LOGGER.debug("*** Entity: " + entity);
        personRepository.save(entity);
    }


    @Override
    public List<Person> findAllByName(String name) throws IOException {
        LOGGER.debug("*** Match By Name Query: name = " + name);
        return personRepository.findAllByName(name);
    }


    @Override
    public List<Person> findAll() throws IOException {
        LOGGER.debug("*** Query = Match all");
        return personRepository.findAll();
    }

}
