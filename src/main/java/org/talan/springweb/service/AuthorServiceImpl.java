package org.talan.springweb.service;

import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;
import org.jboss.logging.Logger;
import org.springframework.transaction.annotation.Transactional;
import org.talan.springdata.config.entity.Author;
import org.talan.springdata.repository.AuthorRepository;
import org.talan.springdata.service.AuthorService;

@Named
public class AuthorServiceImpl implements AuthorService {

    @Inject
    private AuthorRepository authorRepository;

    private static final Logger LOGGER = Logger.getLogger(AuthorRepository.class);


    public AuthorServiceImpl() {
    }


    @Override
    @Transactional
    public void save(Author entity) {
        authorRepository.save(entity);
    }


    @Override
    @Transactional
    public void delete(Author entity) {
        authorRepository.delete(entity);
    }


    @Override
    public List<Author> findAll() {
        LOGGER.debug("*** authorRepository = " + authorRepository);
        return authorRepository.findAll();
    }

}
