package org.talan.springweb.view.bean;

import java.io.IOException;
import java.io.Serializable;
import java.security.Principal;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.portlet.PortletRequest;
import lombok.Getter;
import lombok.Setter;
import org.apache.log4j.Logger;
import org.primefaces.event.RowEditEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.talan.es.service.PersonService;
import org.talan.jest.entity.Person;
import org.talan.springdata.config.entity.Author;
import org.talan.springdata.service.AuthorService;

@Named
@Scope("request")
public class Library implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(Library.class);

    @Inject
    @Getter @Setter
    private PersonService personService;

    @Getter @Setter
    private Person newPerson = new Person();

    @Getter @Setter
    private String searchPersonName;

    @Getter @Setter
    private List<Person> persons;

    @Getter @Setter
    private List<Person> searchPersonsResult;

    ///////////////////////////////////////////////////////
    @Inject
    @Getter @Setter
    private AuthorService authorService;

    @Getter @Setter
    private List<Author> authors;

    @Getter @Setter
    private Author newAuthor = new Author();

    ///////////////////////////////////////////////////////
    @Inject
    @Getter @Setter
    private ApplicationContext context;

    @Getter @Setter
    private Principal principal;

    @Getter @Setter
    private Map<String, String> userInfo;

    @Getter
    private PortletRequest request;

    public Library() {
        LOGGER.debug("*** Constructor");
    }

    @PostConstruct
    public void postConstruct() {
        LOGGER.debug("*** Post constructor");

        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();

        // ============= Portlet 2.0 ============= //
        request = (PortletRequest) externalContext.getRequest();

        // Get The User Role
        if (request.isUserInRole("administrator")) {
            LOGGER.debug("*** User Role = administrator");
        }

        if (request.isUserInRole("guest")) {
            LOGGER.debug("*** User Role = guest");
        }

        if (request.isUserInRole("power-user")) {
            LOGGER.debug("*** User Role = power-user");
        }

        if (request.isUserInRole("user")) {
            LOGGER.debug("*** User Role = user");
        }

        if (request.isUserInRole("library-manager")) {
            LOGGER.debug("*** User Role = library-manager");
        }

        if (request.isUserInRole("auditor")) {
            LOGGER.debug("*** User Role = auditor");
        }

        // Get The User Info
        userInfo = (Map<String, String>) request.getAttribute(PortletRequest.USER_INFO);
        principal = request.getUserPrincipal();

        LOGGER.debug("*** User Info = " + userInfo);
        LOGGER.debug("*** User Auth Type = " + request.getAuthType());
        LOGGER.debug("*** User Remote User = " + request.getRemoteUser());
        LOGGER.debug("*** User Principal = " + principal);
        LOGGER.debug("*** User Principal Class = " + principal.getClass().getTypeName());

        // ============= Spring Data ============= //
        authors = authorService.findAll();
        LOGGER.debug("*** authors = " + authors);

        // ============= ElasticSearch Data ============= //
        try {
            persons = personService.findAll();
            LOGGER.debug("*** Persons = " + persons);
        }
        catch (IOException e) {
            LOGGER.error("*** Error: " + e);
        }
    }


    public void addPerson() {
        LOGGER.debug("*** Person = " + newPerson);

        if (!newPerson.getName().isEmpty() && !newPerson.getAge().isEmpty()) {
            try {
                personService.save(newPerson);
                persons.add(newPerson);
            }
            catch (IOException e) {
                // TODO: show connection error message in Primefaces.
                LOGGER.error("*** Error: " + e);
            }
            // FIXME: ElasticSearch is NRT, so `findAll()` will not return a updated result,
            // FIXME: We must add async call, when the data is updated, will update the View.
        }
    }


    public void searchPersons(AjaxBehaviorEvent event) {
        LOGGER.debug("*** Person Name = " + searchPersonName);

        if ( !searchPersonName.isEmpty() ) {
            try {
                searchPersonsResult = personService.findAllByName(searchPersonName);
            }
            catch (IOException e) {
                // TODO: show connection error message in Primefaces.
                LOGGER.error("*** Error: " + e);
            }
        }
    }


    public void addAuthor() {
        LOGGER.debug("*** auhtor = " + newAuthor);

        if (!newAuthor.getName().isEmpty()) {
            authorService.save(newAuthor);
            authors = authorService.findAll();
            LOGGER.debug("*** Authors = " + authors);
        }
    }


    public void onRowEdit(RowEditEvent event) {
        Author authorToEdit = (Author) event.getObject();

        LOGGER.debug("*** Edited Author = " + authorToEdit);
        LOGGER.debug("*** Authors = " + authors);
        authorService.save(authorToEdit);
    }


    public void onRowEditCancel(RowEditEvent event) {
    }


    public void onRowDelete(Author authorToDelete) {
        LOGGER.debug("*** Removing Author = " + authorToDelete);
        LOGGER.debug("*** Authors = " + authors);
        authorService.delete(authorToDelete);
    }

}
