package org.talan.springweb.config;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.talan.jest.config.JestConfiguration;
import org.talan.springdata.config.PersistenceContext;

@Configuration
@ComponentScan(basePackages = { "org.talan.springweb.service",
                                "org.talan.springweb.view.bean",
                                "org.talan.es.service",
                                "org.talan.jest.repository" })
@Import(value = { PersistenceContext.class,
                  JestConfiguration.class })
public class WebAppInitializer {

    private static final Logger LOGGER = Logger.getLogger(WebAppInitializer.class);

    public WebAppInitializer() {
        LOGGER.debug("*** WebAppInitializer.WebAppInitializer()");
    }

}
