package org.talan.springdata.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.talan.springdata.config.entity.Author;

public interface AuthorRepository extends JpaRepository<Author, Long> {
}
