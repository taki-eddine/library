package org.talan.springdata.config.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@Table(name = "authors")
public class Author implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter @Setter
    private Long id;

    @Column(name = "name", nullable = false)
    @Getter @Setter
    private String name;

    @Version
    @Getter @Setter
    private Long version;

    // TODO: Add method `isValid()`, to check if members fields are ready.

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
