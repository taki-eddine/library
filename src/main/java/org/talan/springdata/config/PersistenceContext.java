package org.talan.springdata.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.util.Properties;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@PropertySource("classpath:application.properties")
@EnableJpaRepositories(basePackages = { "org.talan.springdata.repository" })
@EnableTransactionManagement
public class PersistenceContext {

    @Inject
    private Environment env;

    /**
     * @brief Configures the Data Source Bean.
     * @return Data Source Bean.
     */
    @Bean(destroyMethod = "close")
    public DataSource dataSource() {
        HikariConfig dataSourceConfig = new HikariConfig();

        // Connection
        dataSourceConfig.setDriverClassName(env.getRequiredProperty("db.driver"));
        dataSourceConfig.setJdbcUrl(env.getRequiredProperty("db.url"));

        // Authentication
        dataSourceConfig.setUsername(env.getRequiredProperty("db.username"));
        dataSourceConfig.setPassword(env.getRequiredProperty("db.password"));

        return new HikariDataSource(dataSourceConfig);
    }

    private Properties jpaProperties() {
        Properties properties = new Properties();

        // Configures the used database dialect.
        // This allows Hibernate to create SQL that is optimized for the used
        // database.
        properties.put("hibernate.dialect", env.getRequiredProperty("hibernate.dialect"));

        // Specifies the action that is invoked to the database
        // when the Hibernate SessionFactory is created or closed.
        properties.put("hibernate.hbm2ddl.auto", env.getRequiredProperty("hibernate.hbm2ddl.auto"));

        // Configures the naming strategy that is used
        // when Hibernate creates new database objects and schema elements
        properties.put("hibernate.ejb.naming_strategy", env.getRequiredProperty("hibernate.ejb.naming_strategy"));

        // If the value of this property is true,
        // Hibernate writes all SQL statements to the console.
        properties.put("hibernate.show_sql", env.getRequiredProperty("hibernate.show_sql"));

        // If the value of this property is true,
        // Hibernate will format the SQL that is written to the console.
        properties.put("hibernate.format_sql", env.getRequiredProperty("hibernate.format_sql"));

        return properties;
    }

    /**
     * @brief Configures the Entity Manager Factory Bean.
     * @return Entity Manager Factory Bean.
     */
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();

        // Set Data Source + JPA Vendor Properties
        entityManagerFactoryBean.setDataSource(dataSource());
        entityManagerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter);
        entityManagerFactoryBean.setJpaProperties(jpaProperties());
        entityManagerFactoryBean.setPackagesToScan(getClass().getPackage().getName());

        return entityManagerFactoryBean;
    }

    /**
     * @brief Configures the Transaction Manager Bean.
     * @return Transaction Manger Bean.
     */
    @Bean
    public JpaTransactionManager transactionManager() {
        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return txManager;
    }

}
