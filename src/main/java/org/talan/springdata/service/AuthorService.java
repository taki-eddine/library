package org.talan.springdata.service;

import java.util.List;
import org.talan.springdata.config.entity.Author;

public interface AuthorService {

    void save(Author entity);

    void delete(Author entity);

    List<Author> findAll();

}
